/**
	Modified ring buffer for use in shared objects.

	Authors: Nathan Riddick
	Copyright: 2014 Nathan Riddick
	License: $(LINK2 http://www.boost.org/users/license.html, Boost Software License 1.0)
*/
module handy.sharedringbuffer;
@safe:
import std.algorithm:min;
import std.traits : hasIndirections, isPointer;

bool isPower2()(ulong n) {
	return ( (n != 0) && ( (n & (~n + 1)) == n ) );
}

enum isClass(T) = is(T == class) || is(T == interface);

/**
	Ring buffer for Phaser. Buffer and index are in shared memory, insert method
	is thread-local, chomp method is shared. The buffer must be constructed with
	a capacity of power 2.
*/
struct PhaserRingBuffer(T) {
	static if (isPointer!T) pragma(msg,"you're buffering pointers by the way");
	static if (hasIndirections!T) pragma(msg,"you're buffering pointers by the way");
	static if (isClass!T) pragma(msg,"you're buffering classes(refs) btw");
	
	private shared T[] buffer;

	union {
		private shared size_t _index; //writes to index zero on first insertion
		/// The most recent element's index in the buffer.
		public shared const size_t index;
	}

	/// The capacity of the ring buffer.
	public immutable size_t cap;

	private immutable size_t SIZE_MASK; //power of 2 minus one, for & masking

	@disable this();

	/// Construct a buffer with a capacity of power 2.
	this(size_t n) {
		if (!isPower2(n)) throw new Exception("buffer capacity must be power of 2");
		cap = n;
		SIZE_MASK = n-1;
		buffer.length = cap;
		_index = cap-1;
	}

	/// Stomps data at the rear of the buffer.
	void insert(const T ele) nothrow pure {
		_index = (index+1) & SIZE_MASK; //pow-2 bit masking
		buffer[index] = ele;
	}

	/**
		Inserts the elements in the passed array. Stomps data at the old end of 
		the buffer. 
	
		The Phaser's ring buffer is dependent on the Phaser's put() method 
		filtering large arrays down to the number of first elements that can be 
		safely put, the maximum of which is the buffer's capacity.

		Returns: The number of elements inserted.
	*/
	size_t insert(const T[] es) nothrow pure {
		//auto eles = cast(shared) es;
		auto limit = es.length;
		auto p = es[];

		auto right = (index+1) & SIZE_MASK;
		auto n1 = min(limit,cap-right);
		buffer[right .. right+n1] = p[0 .. n1]; //write right block of array
		auto n2 = limit-n1;
		buffer[0 .. n2] = p[n1 .. $]; //write left block of array

		_index = (index + limit) & SIZE_MASK;

		return limit;
	}

	/// Returns a slice from the right of i to the end of the buffer.
	/// Returns the whole buffer if i is at the end.
	@trusted const(T)[] chomp_right(size_t i) const nothrow shared {
		auto right = (i+1) & SIZE_MASK;
		return cast(const(T)[]) buffer[right..cap];
	}

	/// Returns the complement of chomp_right.
	@trusted const(T)[] chomp_left(size_t i) const nothrow shared {
		auto right = (i+1) & SIZE_MASK;
		return cast(const(T)[]) buffer[0..right];
	}
}