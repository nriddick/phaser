
/**
	Single publisher Disruptor around a ring buffer.

	Authors: Nathan Riddick
	Copyright: 2014 Nathan Riddick
	License: $(LINK2 http://www.boost.org/users/license.html, Boost Software License 1.0)
*/
module handy.phaser;
@safe:
public import handy.sharedringbuffer;
public import handy.subscriber;
version(unittest) import handy.ptests;
import std.algorithm;
import core.atomic;

/**
	Single producer Disruptor around a ring buffer.

	Passes data to subscribers which may be on separate threads. Operations 
	beyond setup are lock-free, and main loop operations do not allocate.
	
	Only capacities that are a power of 2 are valid.
	
	When subscribers fall behind, further insertions are dropped. It is the 
	producer's responsibility to structure further fallback behavior.

	Optimal parameters may be highly dependent on the system and workload.

	Examples:
	---
	void foo(Subscriber!int sub) {
		import std.array:appender;
		auto recv = appender!(int[])();
		while(true) {
			sub.pull(recv);
			foreach(n; recv.data) if (n == 42) return;
		}
	}

	{
		import std.concurrency:spawn;
		auto phaser = new Phaser!int(2^^8);
		std.concurrency.spawn(&foo, phaser.subscribe);
		phaser.put(42);
	}


	void goo(Subscriber!int sub) {
		while(true) {
			auto r = range(sub);
			if (r.front == 43) {
				sub.advance(r.length);
				
				auto caught = false;
				try { r.front; }
				catch (Exception e) {
					caught = true;
					if (e.msg != "sequence invalidated")
						assert(false,"unexpected exception");
				}
				if (!caught) assert(false,"expected invalid sequence");
				return;
			}
		}
	}

	{
		import std.concurrency:spawn;
		auto phaser = new Phaser!int(2^^8);
		std.concurrency.spawn(&goo, phaser.subscribe);
		phaser.put(43);
	}

	See_Also:
	Feed, Subscriber
	---
*/
//struct Phaser(T) {
final class Phaser(T) {
	private PhaserRingBuffer!(T) buf;
	private immutable size_t cap;
	private shared size_t sequence = size_t.max; //first write to sequence 0
	private shared size_t[] subscribers; //subscribers' latest sequence
	private shared int num_subs;
	private size_t[] subs_buf; //reduce safeslots() GC/alloc pressure
	private long safeslots_cache; //cache safeslots to reduce repeated calls

	/// The number of current subscribers.
	auto numSubscribers() const @nogc { 
		return num_subs;
	}

	@disable this();

	/// Instantiate w/ the max number of subscribers, 128 by default.
	this(int c, int max_subs = 128) {
		buf = PhaserRingBuffer!T(c);
		cap = c;
		subscribers.length = max_subs;
		subs_buf.length = max_subs;
		num_subs = 0;
		version(unittest) sequence = size_t.max - cap; //test sequence wrap
		//sequence must start on size_t.max +/- n*cap because it also reflects ring index
	}

/**
	Returns the appropriate Subscriber which may be passed to another thread.

	Returns: Subscriber!T(this, num_subs-1, subscribers[num_subs-1])

	See_Also: Subscriber
*/
	@trusted Subscriber!T subscribe() {	
		if (num_subs >= subscribers.length)
			throw new Exception("maximum subscribers reached");
		subscribers[num_subs] = sequence;
		auto index = num_subs;
		num_subs.atomicOp!"+="(1);
		return Subscriber!T(
			this,
			index,
			subscribers[index]);
	}

/**
	Returns the current sequence and slices to contiguous parts of the buffer
	left and right of the corresponding index.
	Generally called by Subscriber's chomp().
	The Subscriber determines which parts it hasn't consumed.
	
	Returns: tuple(sequence, olderSlice, newerSlice)
*/
	Tuple!(size_t, const(T)[], const(T)[]) chomp() const shared nothrow @nogc
	{
		size_t masterSeq = sequence;

		auto r = buf.chomp_right(masterSeq); //older data
		auto l = buf.chomp_left(masterSeq); //newer data
		
		return tuple(masterSeq, r, l);
	}

/**
	Called by Subscriber.chomp to announce how many elements the Subscriber has
	consumed.

	Called implicitly by Subscriber.pull which copies the data into another
	range instead of returning slices to the Phaser's buffer.

	Examples:
	---
	import std.exception:enforce;
	import std.array:appender;
	auto phaser = new Phaser!char(1); //buffer size one
	auto sub = phaser.subscribe;
	auto buf = appender!(char[])();

	phaser.put('a');
	enforce(phaser.put('b') == false); //subscriber not caught up, no write
	auto bite = sub.chomp;
	foreach(slice; bite) buf ~= slice;
	sub.advance(bite.chompLength);

	enforce(buf.data[0] == 'a');
	enforce(phaser.put('b') == true);

	sub.pull(buf);
	enforce(buf.data[1] == 'b');
	enforce(phaser.put('c') == true);
	---
*/
	void advance(size_t index, size_t inc) shared nothrow @nogc {
		size_t v = subscribers[index];
		subscribers[index] = v + inc;
	}

	/// Returns: true if the element was inserted, or false if there were no
	/// available slots.
	@trusted bool put(const T ele) nothrow @nogc {
		if (safeslots_cache < 1) {
			auto x = safeslots;
			safeslots_cache = x;
			if (x < 1) return false;
		}
		buf.insert(ele);
		version (X86_64) {
			auto v = sequence+1;
			sequence = v;
		}
		else {
			auto v = sequence + 1;
			sequence.atomicStore(v);
		}
		--safeslots_cache;
		return true;
	}

	/// Puts as many elements as will safely fit without stomping unconsumed data.
	/// Returns: the number of elements inserted.
	@trusted size_t put(const T[] eles) nothrow @nogc {
		auto limit = min(eles.length,safeslots);
		if (limit == 0) return 0;
		auto p = eles[0..limit];
		buf.insert(p);
		version(X86_64) {
			auto v = sequence+limit;
			sequence = v;
		}
		else {
			auto v = sequence+limit;
			sequence.atomicStore(v);
		}
		safeslots_cache -= limit;
		return limit;
	}

	size_t put(R)(R range) {

	}

	/// Returns: How many elements could be inserted without stomping unread
	/// data. Zero if there are no subscribers.
	size_t safeslots() @nogc {
		auto subsct = num_subs;
		if (subsct == 0) return 0;

		auto seq = sequence;
		subs_buf[0..subsct] = subscribers[0..subsct];
		auto active_slice = subs_buf[0..subsct];

		if (active_slice.nothrowReduceMax > seq) { // sequence wrapped
			active_slice[] += cap;
			auto march = active_slice.nothrowReduceMin;
			return march - seq;
		}
		else {
			return cap - (seq - active_slice.nothrowReduceMin);
		}
	}

}
private size_t nothrowReduceMax(R)(R stuff) nothrow @nogc {
	size_t ret = size_t.min;
	foreach (e; stuff) ret = max(ret,e);
	return ret;
}
private size_t nothrowReduceMin(R)(R stuff) nothrow @nogc {
	size_t ret = size_t.max;
	foreach (e; stuff) ret = min(ret,e);
	return ret;
}
