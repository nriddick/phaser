/**
	Subscription object for receiving data from a Phaser.

	Authors: Nathan Riddick
	Copyright: 2014 Nathan Riddick
	License: $(LINK2 http://www.boost.org/users/license.html, Boost Software License 1.0)
*/
module handy.subscriber;
@safe:
import std.typecons : Tuple, tuple;
import std.range : isOutputRange;
import handy.phaser;

/**
	A subscriber to a Phaser of type T.

	Default construction is illegal.
*/
struct Subscriber(T) {
	private shared Phaser!T handle;
	
	/// The Subscriber's index in its Phaser.
	union {
		const size_t index;
		private size_t _index;
	}

	union {
		private size_t _readerSeq;
		
		/// The Subscriber's current sequence.
		public const size_t readerSeq;
	}

	//@disable this();

	/// Construct with Phaser's current state
	@trusted this(Phaser!T r, size_t i, size_t seq)
	{
		handle = cast(shared) r;
		_index = i;
		_readerSeq = seq;
	}

	/// Returns two arrays to fresh contiguous data, where the first array is older than the last.
	@trusted Tuple!(const(T)[], const(T)[]) chomp() const {
		auto x = handle.chomp();
		auto older = x[1];
		auto newer = x[2];
		auto masterSeq = x[0];

		size_t behind;
		if (readerSeq > masterSeq) { //sequence wrapped
			behind = (0-readerSeq) + masterSeq;
		}
		else behind = masterSeq - readerSeq;

		if (newer.length >= behind) { //newer part of buffer has all the new data
			newer = newer[$-behind .. $];
			older = (const(T)[]).init;
		}
		else { //else newer part has some, the rest is in the older part
			auto oldtake = behind - newer.length;
			older = older[$-oldtake .. $];
		}

		import core.atomic:atomicFence;
		atomicFence();
		return tuple(older,newer);
	}

	/// Increments locally stored sequence and advances the feed.
	void advance(size_t inc) {
		if (inc>0) {
			_readerSeq += inc;
			handle.advance(index,inc);
		}
	}
}

/**
	chomp() result chained into range. Remember the following:
		
		The result will probably be invalid or even corrupt if you use 
		it after advance()'ing the Subscriber it was generated from.
		
		Multiple calls to raw_range() may be not be equal even without an advance() 
		because the Subscriber's paired Phaser might still have safeslots left.
*/
auto raw_range(T)(const Subscriber!T sub) {
	import std.range:chain;
	auto x = sub.chomp();
	return chain(x[0],x[1]);
}

/**
	A sequence-aware input range for a Subscriber's chomp() result. Throws an 
	Exception if the range methods are called after the Subscriber has been 
	advance()'d.
*/
@trusted struct sub_range(T) {
	import std.traits:ReturnType;
	private ReturnType!(raw_range!T) r;
	union {
		private Subscriber!T* _s;
		private const Subscriber!T* s;
	}
	union {
		const size_t seq;
		private size_t _seq;
	}
	
	@disable this();
	this(ref Subscriber!T sub) {
		_seq = sub.readerSeq;
		r = raw_range(sub);
		_s = &sub;
	}

	auto front() {
		if (seq != s.readerSeq) throw new Exception("sequence invalidated");
		return r.front;
	}

	void popFront() {
		if (seq != s.readerSeq) throw new Exception("sequence invalidated");
		r.popFront;
	}

	auto empty() {
		if (seq != s.readerSeq) throw new Exception("sequence invalidated");
		return r.empty;
	}

	auto length() {
		if (seq != s.readerSeq) throw new Exception("sequence invalidated");
		return r.length;		
	}
}

/// Helper factory function to return a sub_range.
auto range(T)(ref Subscriber!T sub) {
	return sub_range!T(sub);
}

/// All in one chomp, put, and advance copying into an output range.
/// Returns: The number of elements pulled.
size_t pull(R,T)(ref Subscriber!T sub, ref R range) if (isOutputRange!(R,T))
{
	auto x = sub.chomp();
	range.put(x[0]);
	range.put(x[1]);
	sub.advance(x.chompLength);
	return x.chompLength;
}

/// Total length of a Subscriber chomp
size_t chompLength(T)( const Tuple!(const(T)[], const(T)[]) bite ) {
	return bite[0].length + bite[1].length;
}

/**
	Returns a range to a Subscriber's chomp. It waits up to the given Duration 
	for the range to have a non-zero length.
*/
import core.time:Duration;
auto receiveTimeout_phaser(T)(Duration d, Subscriber!T s) {
	import std.datetime:StopWatch;
	StopWatch sanity;
	sanity.start;
	auto x = s.range;
	while (x.length == 0) {
		x = s.range;
		if (cast(Duration) sanity.peek > d) break;
	}
	return x;
}
