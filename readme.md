A lock-free, concurrent 1:N buffer based on the LMAX Disruptor pattern. 
Low latency, high throughput. I essentially reduced the Disruptor to the 
functionality which I found personally useful and called it a Phaser.

Here's some dubiously useful performance numbers, tested on an i5 2500K.

```
LDC -O -release
Using array put(), buffering data on receiver threads
	secs put 8388608 ints thru buflen 65536 (factor 128), 1 threads:
		avg:0.00668231 min:0.00661193 max:0.00677241
	avg 1.25535e+09 ints per second

	secs put 8388608 ints thru buflen 65536 (factor 128), 3 threads:
		avg:0.0138625 min:0.0136111 max:0.0147574
	avg 6.05128e+08 ints per second



Using single put(), buffering data on receiver threads
	secs put 8388608 ints thru buflen 65536 (factor 128), 1 threads:
		avg:0.317211 min:0.311749 max:0.325493
	avg 2.64449e+07 ints per second (~38ns avg put latency)

	secs put 8388608 ints thru buflen 65536 (factor 128), 3 threads:
		avg:0.348801 min:0.342267 max:0.359803
	avg 2.40498e+07 ints per second (~42ns avg put latency)


Using array put(), performing sum of data on receiver threads
	num int's put in buflen 65536 (samplelen 21845, factor 0.333328), 1 threads, 1 seconds:
		avg:5.15533e+09 min:5.08948e+09 max:5.17185e+09
	avg 5.15 billion ints per second

	num int's put in buflen 65536 (samplelen 21845, factor 0.333328), 3 threads, 1 seconds:
		avg:4.68135e+09 min:4.54085e+09 max:4.76287e+09
	avg 4.68 billion ints per second


Using single put(), performing sum of data on receiver threads
	num int's put in buflen 65536 (samplelen 21845, factor 0.333328), 1 threads, 1 seconds:
		avg:2.75945e+07 min:2.67699e+07 max:2.86621e+07
	avg 27.6 million puts per second (mean latency ~36ns)

	num int's put in buflen 65536 (samplelen 21845, factor 0.333328), 3 threads, 1 seconds:
		avg:2.40215e+07 min:2.34694e+07 max:2.43921e+07
	avg 24.0 million puts per second (mean latency ~42ns)


Using array put(), adding length of data on receiver threads
	num int's put in buflen 65536 (samplelen 21845, factor 0.333328), 1 threads, 1 seconds:
		avg:6.86283e+09 min:6.75093e+09 max:6.91007e+09
	avg 6.86 billion ints per second

	num int's put in buflen 65536 (samplelen 21845, factor 0.333328), 3 threads, 1 seconds:
		avg:6.43594e+09 min:6.27508e+09 max:6.55218e+09
	avg 6.43 billion ints per second

DMD -O -inline -release
Using array put(), buffering data on receiver threads
	secs put 8388608 ints thru buflen 65536 (factor 128), 1 threads:
		avg:0.00702152 min:0.00681906 max:0.00728743
	avg 1.19 billion ints per second

	secs put 8388608 ints thru buflen 65536 (factor 128), 3 threads:
		avg:0.0192005 min:0.0155629 max:0.0364483
	avg 0.437 billion ints per second

Using single put(), buffering data on receiver threads
	secs put 8388608 ints thru buflen 65536 (factor 128), 1 threads:
		avg:0.338107 min:0.335821 max:0.341402
	avg 24.81 million puts per second (mean latency ~40ns)

	secs put 8388608 ints thru buflen 65536 (factor 128), 3 threads:
		avg:0.408051 min:0.391261 max:0.414611
	avg 20.56 million puts per second (mean latency ~49ns)	
```

Some notes on the unittests: They make several big allocations. Depending on
your RAM, resource usage, OS and which compiler you use you might see out of
memory errors and/or deadlocks due to needing contiguous memory. On Win64, 
compiling with DMD in 64bit (compiler switch -m64) works around this because it 
uses Microsoft's runtime. Otherwise, reduce the size of the sample array.